compose:
	docker-compose -f ./raspi-runners.yml -p raspi-runners --verbose up -d

jdk11:
	docker build -t raspi-jdk11-runner -f raspi-jdk11-runner/Dockerfile .

texlive:
	docker build -t raspi-texlive-runner -f raspi-texlive-runner/Dockerfile .